#!/bin/bash 

set -e 

if [ "$(docker ps -q -f name=flask)" ]; then
    docker rm -f flask
    docker rmi -f $(docker images -q)
fi

if [ ! "$(docker ps -q -f name=flask)" ]; then
    docker run -p 8080:8080 -d --name flask nicolas/flask:1.0
fi