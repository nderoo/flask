#!/bin/bash

set -e


mkdir -p ~/.ssh # on cree le dossier
touch ~/.ssh/id_rsa
echo -e "$AWS_KEY" > chmod 600 ~/.ssh/id_rsa

# 2 : On change la configuration de ssh 
touch ~/.ssh/config # On crée un fichier de config 
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config # On désactive le Key checking

ssh ec2-user@$AWS_IP bash < ./scripts/updateAndRestart.sh